/**
 * Counts the number of tasks for the session
 * @param session Session to count tasks
 */
export function countTasks(session): number {
  let tasks = 0;
  // Count number of tasks for session
  session.result.tasks.forEach(task => {
    if (task !== '') {
      tasks++;
    }
  });

  return tasks;
}


/**
 * Returns milliseconds in hour format
 * @param input time to format
 */
export function getTimeFormatted(input: number) {
  // tslint:disable-next-line:one-variable-per-declaration
  let totalHours, totalMinutes, totalSeconds, hours, minutes, seconds, result = '';
  totalSeconds = input / 1000;
  totalMinutes = totalSeconds / 60;
  totalHours = totalMinutes / 60;

  seconds = Math.floor(totalSeconds) % 60;
  minutes = Math.floor(totalMinutes) % 60;
  hours = Math.floor(totalHours) % 60;

  if (hours !== 0) {
    result += hours + ':';
    if (minutes.toString().length === 1) {
      minutes = '0' + minutes;
    }
  }
  result += minutes + ':';
  if (seconds.toString().length === 1) {
    seconds = '0' + seconds;
  }
  result += seconds;
  return result;
}


