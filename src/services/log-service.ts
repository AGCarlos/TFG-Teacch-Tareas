import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from './user-service';

/**
 * Log service
 */
@Injectable({
  providedIn: 'root'
})
export class LogService {

  /**
   * API url
   */
  apiUrl = 'http://127.0.0.1:5000/';

  header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.userService.getToken()}`)
  };

  constructor(
    protected http: HttpClient,
    private userService: UserService
  ) {
  }

  /**
   * Creates a log
   * @param log log attributes
   */
  createLog(log): Observable<any> {
    return this.http.post(this.apiUrl + 'log', {new_log: log}, this.header);
  }

  /**
   * Lists all the logs
   */
  getLogs(id): Observable<any> {
    return this.http.get(this.apiUrl + 'log/' + id.$oid, this.header);
  }

  /**
   * Deletes a log
   * @param id log id to delete
   */
  deleteLogs(): Observable<any> {
    return this.http.delete(this.apiUrl + '/log', this.header);
  }


}
