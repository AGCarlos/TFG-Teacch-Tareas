import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {UserData} from '../interfaces/interfaces.interfaces';

/**
 * User service
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  /**
   * API url
   */
  apiUrl = 'http://127.0.0.1:5000/';

  user: UserData;

  header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.getToken()}`)
  };

  constructor(
    protected http: HttpClient,
    private cookies: CookieService
  ) {
  }

  /**
   * Set user token
   */
  setToken(token: string) {
    this.cookies.set('token', token);
  }

  /**
   * Get user token
   */
  getToken() {
    return this.cookies.get('token');
  }

  /**
   * User login
   * @param user User to login
   */
  login(user: any): Observable<any> {
    return this.http.post(this.apiUrl + 'user/login', {user});
  }

  /**
   * User register
   * @param user User to register
   */
  register(user: any): Observable<any> {
    return this.http.post(this.apiUrl + 'user/register', {user});
  }

  /**
   * Return logged user
   */
  getUserLogged(): Observable<any> {
    return this.http.get(this.apiUrl + 'user_token', this.header);
  }

  /**
   * Creates a user
   * @param user user attributes
   */
  createUser(user): Observable<any> {
    return this.http.post(this.apiUrl + 'user', {new_user: user}, this.header);
  }

  /**
   * Returns a user
   * @param id user id to retrieve
   */
  getUser(id): Observable<any> {
    return this.http.get(this.apiUrl + 'user/' + id, this.header);
  }

  /**
   * Lists all the users
   */
  getUsers(): Observable<any> {
    return this.http.get(this.apiUrl + 'user', this.header);
  }


  /**
   * Updates a user
   * @param id user id to update
   * @param userData new user data to update
   */
  updateUser(id, userData): Observable<any> {
    return this.http.put(this.apiUrl + 'user/' + id, {user_data: userData}, this.header);
  }

  /**
   * Deletes a user
   * @param id user id to delete
   */
  deleteUser(id): Observable<any> {
    return this.http.delete(this.apiUrl + '/user/' + id, this.header);
  }


}
