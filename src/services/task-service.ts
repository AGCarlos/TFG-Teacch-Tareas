import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from './user-service';

/**
 * Task service
 */
@Injectable({
  providedIn: 'root'
})
export class TaskService {

  /**
   * API url
   */
  apiUrl = 'http://127.0.0.1:5000/';

  header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.userService.getToken()}`)
  };

  constructor(
    protected http: HttpClient,
    private userService: UserService
  ) {
  }

  /**
   * Creates a task
   * @param task task attributes
   */
  createTask(task): Observable<any> {
    return this.http.post(this.apiUrl + 'task', {new_task: task}, this.header);
  }

  /**
   * Returns a task
   * @param id task id to retrieve
   */
  getTask(id): Observable<any> {
    return this.http.get(this.apiUrl + 'task/' + id, this.header);
  }

  /**
   * Lists all the tasks
   */
  getTasks(): Observable<any> {
    return this.http.get(this.apiUrl + 'task', this.header);
  }

  /**
   * Lists all the tasks
   */
  getSessionTasks(ids): Observable<any> {
    return this.http.put(this.apiUrl + 'tasks', {ids}, this.header);
  }


  /**
   * Updates a task
   * @param id task id to update
   * @param taskData new task data to update
   */
  updateTask(id, taskData): Observable<any> {
    return this.http.put(this.apiUrl + 'task/' + id, {task_data: taskData}, this.header);
  }

  /**
   * Deletes a task
   * @param id task id to delete
   */
  deleteTask(id): Observable<any> {
    return this.http.delete(this.apiUrl + '/task/' + id, this.header);
  }


}
