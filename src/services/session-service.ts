import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from './user-service';

/**
 * Session service
 */
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  /**
   * API url
   */
  apiUrl = 'http://127.0.0.1:5000/';

  /**
   * Actual session id
   */
  id: string;

  header = {
    headers: new HttpHeaders()
      .set('Authorization', `Bearer ${this.userService.getToken()}`)
  };

  constructor(
    protected http: HttpClient,
    private userService: UserService
  ) {
  }

  /**
   * Creates a session
   * @param session session attributes
   */
  createSession(session): Observable<any> {
    return this.http.post(this.apiUrl + 'session', {new_session: session}, this.header);
  }

  /**
   * Updates a session
   * @param id session id to update
   * @param sessionData new session data to update
   */
  updateSession(id, sessionData): Observable<any> {
    return this.http.put(this.apiUrl + 'session/' + id, {session_data: sessionData}, this.header);
  }

  /**
   * Returns a session
   * @param id session id to retrieve
   */
  getSession(id): Observable<any> {
    return this.http.get(this.apiUrl + 'session/' + id.$oid, this.header);
  }

  /**
   * Lists all the sessions
   */
  getSessions(): Observable<any> {
    return this.http.get(this.apiUrl + 'session', this.header);
  }

  /**
   * Deletes a session
   * @param id session id to delete
   */
  deleteSession(id): Observable<any> {
    return this.http.delete(this.apiUrl + '/session/' + id, this.header);
  }


}
