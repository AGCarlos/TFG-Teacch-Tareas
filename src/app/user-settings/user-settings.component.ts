import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {CreateUserDialogComponent} from '../create-user-dialog/create-user-dialog.component';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UpdateUserDialogComponent} from '../update-user-dialog/update-user-dialog.component';
import {UserData} from '../../interfaces/interfaces.interfaces';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit {

  /**
   * List of users
   */
  users: UserData[];

  displayedColumns: string[] = ['image', 'name', 'age', 'actions'];
  dataSource = new MatTableDataSource<UserData>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private userService: UserService, public dialog: MatDialog,
              private snackBar: MatSnackBar, private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getUserList();
  }

  createUser() {
    const dialogRef = this.dialog.open(CreateUserDialogComponent, {
      width: '500px',
      hasBackdrop: true,
      data: {}
    });

    // Add user to DB
    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        let teacherId = '';
        this.userService.getUserLogged().subscribe(res => {
          teacherId = res.result._id.$oid;

          data.createdAt = this.getTime();
          data.teacherId = teacherId;
          data.calendar = [[], [], [], [], [], [], []];
          this.userService.createUser(data).subscribe(() => {
              this.getUserList();
              this.snackBar.open('Usuario creado correctamente', '', {duration: 2000});
            },
            error => {
              console.log('Error:' + error.statusText);
            });
        });
      }
    });
  }

  updateUser(user: UserData, id: any) {
    const dialogRef = this.dialog.open(UpdateUserDialogComponent, {
      height: '600px',
      width: '500px',
      hasBackdrop: true,
      data: {name: user.name, age: user.age, image: user.image}
    });

    // Add user to DB
    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        data.lastModifiedAt = this.getTime();
        this.userService.updateUser(id.$oid, data).subscribe(() => {
            this.getUserList();
            this.snackBar.open('Usuario editado correctamente', '', {duration: 2000});
          },
          error => {
            console.log('Error:' + error.statusText);
          });
      }
    });
  }

  deleteUser(id: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
    });

    dialogRef.componentInstance.title = 'Confirmar eliminación de usuario';
    dialogRef.componentInstance.message = 'Está seguro de querer eliminar el usuario ?';

    // Delete user from DB
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.userService.deleteUser(id.$oid).subscribe(result => {
            if (result.deleted) {
              this.users.splice(this.users.findIndex(item => item._id === id), 1);
              this.snackBar.open('Usuario eliminado correctamente', '', {duration: 2000});
              this.getUserList();
            }
          },
          error => {
            console.log('Error:' + error.statusText);
          });
      }
    });
  }

  /**
   * Retrieves user list from API
   */
  getUserList() {
    this.userService.getUsers().subscribe(users => {
        this.users = users.result;
        this.dataSource = new MatTableDataSource(this.users);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.paginator._intl.itemsPerPageLabel = 'Usuarios por página';
        });
        this.dataSource.filterPredicate = (data, filter: string): boolean => {
          return data.name.toLowerCase().includes(filter);
        };
      },
      error => {
        console.log('Error:' + error.statusText);
      });
  }

  /**
   * Applies filters to table
   * @param event filter event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * Gets actual time
   */
  getTime() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    return date + ' ' + time;
  }
}
