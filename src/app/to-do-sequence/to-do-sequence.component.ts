import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';
import {SessionService} from '../../services/session-service';
import {SessionData, TaskClick, TaskData} from '../../interfaces/interfaces.interfaces';
import {TaskService} from '../../services/task-service';

@Component({
  selector: 'app-to-do-sequence',
  templateUrl: './to-do-sequence.component.html',
  styleUrls: ['./to-do-sequence.component.scss']
})
export class ToDoSequenceComponent implements OnInit {

  /**
   * Emits when dropped
   */
  @Output() dropped = new EventEmitter<number>();

  /**
   * Emits when task is clicked
   */
  @Output() taskClicked: EventEmitter<TaskClick> = new EventEmitter();

  /**
   * Whether every task is active or not
   */
  activeTasks = [true, true, true, true];

  /**
   * List of tasks
   */
  tasks: TaskData[] = [];

  /**
   * Connect list for the to-do list
   */
  connectList: string[];

  /**
   * Activate effects
   */
  activate = true;

  /**
   * Whether the sounds must be played
   */
  session: SessionData;

  constructor(private taskService: TaskService, private sessionService: SessionService, private router: Router) {
    this.connectList = ['workingZone', '', '', ''];
  }

  ngOnInit() {
    if (this.sessionService.id) {
      this.sessionService.getSession(this.sessionService.id).subscribe(session => {
        this.session = session.result;
        this.taskService.getSessionTasks(session.result.tasks.filter(e => e)).subscribe(tasks => {
          session.result.tasks.forEach((value, index) => {
            tasks.result.forEach(val => {
              if (val._id.$oid === value) {
                this.tasks.push(val);
              }
            });
          });
        });
      });
    } else {
      this.router.navigateByUrl('/');
    }
  }

  /**
   * Drop function
   * @param event drop event
   */
  drop(event: CdkDragDrop<TaskData, any>, num: number) {
    this.activate = true;
    if (event.previousContainer !== event.container) {
      // Set the actual task
      this.activeTasks[num] = true;
      // Disable the task on dashboard
      this.dropped.emit(num);
    }
  }

  /**
   * Sets the opacity for disabled tasks
   * @param i task number
   */
  setOpacity(i: number) {
    return !(this.connectList[i] === 'workingZone') ? 0.1 : 1;
  }

  hasAnimation(i) {
    return this.activate && ((this.activeTasks[0] && i === 0) || !this.activeTasks[i - 1]) && !this.session.animationsDisabled;
  }

  dragDisabled() {
    return this.session.selection !== 'drag';
  }

  taskClick(task, n) {
    if (this.session.selection === 'click') {
      this.taskClicked.emit({
        task,
        number: n
      });
    }
  }
}
