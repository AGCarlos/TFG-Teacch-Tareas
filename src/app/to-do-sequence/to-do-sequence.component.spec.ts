import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ToDoSequenceComponent} from './to-do-sequence.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('ToDosComponent', () => {
  let component: ToDoSequenceComponent;
  let fixture: ComponentFixture<ToDoSequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToDoSequenceComponent],
      imports: [DragDropModule, HttpClientTestingModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
