import {FormControl, Validators} from '@angular/forms';
import {Component} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  name = '';
  email = '';
  password = '';
  confirmPassword = '';
  error: boolean;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(
    private userService: UserService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  register() {
    if (this.password !== this.confirmPassword) {
      this.error = true;
    } else {
      const user = {
        name: this.name,
        email: this.email,
        password: this.password
      };
      this.userService.register(user).subscribe(res => {
          this.snackBar.open('Usuario registrado, ya puede iniciar sesión', '', {duration: 3000});
          this.router.navigateByUrl('/login');
        },
        error => {
          this.snackBar.open('Error al registrarse. Inténtelo de nuevo', '', {duration: 3000});
        });
    }
  }

}
