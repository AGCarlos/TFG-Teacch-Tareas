import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserData} from '../../interfaces/interfaces.interfaces';

@Component({
  selector: 'app-update-user-dialog',
  templateUrl: './update-user-dialog.component.html',
  styleUrls: ['./update-user-dialog.component.scss']
})
export class UpdateUserDialogComponent implements OnInit {

name: string;
  age: number;
  role: string;
  password: string;
  confirmPassword: string;
  image: string;

  constructor(public dialogRef: MatDialogRef<UpdateUserDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UserData) {
  }

  ngOnInit() {
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close();
  }

  onFileSelected(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.image = reader.result as ArrayBuffer;
      };
    }
  }

  validate(role: string) {
    let invalid = false;

    if (role === 'profesor') {
      if (!this.data.name || !this.data.age || !this.data.role || !this.data.password) {
        invalid = true;
      } else if (!this.data.password !== !this.confirmPassword) {
        invalid = true;
      }
    } else if (role === 'estudiante') {
      if (!this.data.name || !this.data.age || !this.data.role) {
        invalid = true;
      }
    }

    return invalid;
  }
}
