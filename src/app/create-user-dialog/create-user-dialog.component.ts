import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UserData} from '../../interfaces/interfaces.interfaces';

@Component({
  selector: 'app-create-user-dialog',
  templateUrl: './create-user-dialog.component.html',
  styleUrls: ['./create-user-dialog.component.scss']
})

export class CreateUserDialogComponent implements OnInit {
  name: string;
  age: number;
  image: string;

  constructor(public dialogRef: MatDialogRef<CreateUserDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UserData) {
  }

  ngOnInit() {
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close();
  }

  onFileSelected(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.image = reader.result as ArrayBuffer;
      };
    }
  }
}
