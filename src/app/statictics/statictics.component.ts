import {Component, OnInit, ViewChild} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {Label} from 'ng2-charts';
import {LogData, UserData} from '../../interfaces/interfaces.interfaces';
import {LogService} from '../../services/log-service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {UserService} from '../../services/user-service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-statictics',
  templateUrl: './statictics.component.html',
  styleUrls: ['./statictics.component.scss']
})
export class StaticticsComponent implements OnInit {

  /**
   * Displayed columns for table
   */
  displayedColumns: string[] = ['session_name', 'time', 'date'];

  /**
   * Table datasource
   */
  dataSource = new MatTableDataSource<LogData>([]);

  /**
   * Total students
   */
  private students: UserData[] = [];

  // Chart configuration variables
  sessionNames = [];
  sessionCounts = [];
  barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    tooltips: {titleFontSize: 0}
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [];
  dataLoaded = false;

  /**
   * Student selector variables
   */
  public studentFilterCtrl: FormControl = new FormControl();
  public filteredStudents: ReplaySubject<UserData[]> = new ReplaySubject<UserData[]>(1);
  protected onDestroy = new Subject<void>();
  public studentForm: FormGroup;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private logService: LogService,
    private formBuilder: FormBuilder,
    private userService: UserService,
  ) {
    this.studentForm = this.formBuilder.group({
      student: ['']
    });
  }

  ngOnInit(): void {
    // Retrieve students
    this.userService.getUsers().subscribe(res => {
      this.students = res.result;
    });

    this.filteredStudents.next(this.students.slice());

    this.studentFilterCtrl.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterStudents();
      });
  }

  /**
   * Returns the log list and fills the table
   */
  private getLogList(id) {
    this.logService.getLogs(id).subscribe(res => {
      this.barChartData = [];
      this.barChartLabels = [];
      this.getSessionsCount(res.result);
      this.dataSource = new MatTableDataSource(res.result);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.paginator._intl.itemsPerPageLabel = 'Sesiones por página';
      });
    });
  }

  /**
   * Gets the sesion count for each session
   * @param logs Logs to count
   */
  getSessionsCount(logs: LogData[]) {
    logs.forEach(log => {
      if (this.sessionNames.includes(log.session_name)) {
        const index = this.sessionNames.indexOf(log.session_name);
        this.sessionCounts[index]++;
      } else {
        this.sessionNames.push(log.session_name);
        this.sessionCounts.push(1);
      }
    });
    this.sessionNames.forEach((session, i) => {
      this.barChartData.push({data: [this.sessionCounts[i]], label: session});
    });
    this.dataLoaded = true;
  }

  /**
   * Filters the student list
   */
  protected filterStudents() {
    if (!this.students) {
      return;
    }
    // get the search keyword
    let search = this.studentFilterCtrl.value;
    if (!search) {
      this.filteredStudents.next(this.students.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredStudents.next(
      this.students.filter(user => user.name.toLowerCase().indexOf(search) > -1)
    );
  }

  changeStats() {
    const id = this.studentForm.get('student').value._id;
    this.getLogList(id);
  }
}
