import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {CreateTaskDialogComponent} from '../create-task-dialog/create-task-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TaskData} from '../../interfaces/interfaces.interfaces';
import {TaskService} from '../../services/task-service';
import {UpdateTaskDialogComponent} from '../update-task-dialog/update-task-dialog.component';
import {Router} from '@angular/router';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-task-settings',
  templateUrl: './task-settings.component.html',
  styleUrls: ['./task-settings.component.scss'],
})
export class TaskSettingsComponent implements OnInit {

  /**
   * List of tasks
   */
  tasks: TaskData[];

  displayedColumns: string[] = ['image', 'name', 'actions'];
  dataSource = new MatTableDataSource<TaskData>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private taskService: TaskService, public dialog: MatDialog,
              private snackBar: MatSnackBar, private router: Router,
              private userService: UserService
  ) {
  }

  ngOnInit() {
    this.getTaskList();
  }

  createTask() {
    const dialogRef = this.dialog.open(CreateTaskDialogComponent, {
      width: '500px',
      hasBackdrop: true,
      data: {}
    });

    // Add task to DB
    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        let teacherId = '';
        this.userService.getUserLogged().subscribe(res => {
          teacherId = res.result._id.$oid;

          data.createdAt = this.getTime();
          data.teacherId = teacherId;
          this.taskService.createTask(data).subscribe(() => {
              this.getTaskList();
              this.snackBar.open('Tarea creada correctamente', '', {duration: 2000});
            },
            error => {
              console.log('Error:' + error.statusText);
            });
        });

      }
    });
  }

  updateTask(task: TaskData, taskID: any) {
    const dialogRef = this.dialog.open(UpdateTaskDialogComponent, {
      height: '600px',
      width: '500px',
      hasBackdrop: true,
      data: {name: task.name, image: task.image, helpPictos: task.helpPictos}
    });


    // Add task to DB
    dialogRef.afterClosed().subscribe(data => {
      if (data !== undefined) {
        data.lastModifiedAt = this.getTime();
        this.taskService.updateTask(taskID.$oid, data).subscribe(() => {
            this.getTaskList();
            this.snackBar.open('Tarea editada correctamente', '', {duration: 2000});
          },
          error => {
            console.log('Error:' + error.statusText);
          });
      }
    });
  }

  deleteTask(id: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
    });

    dialogRef.componentInstance.title = 'Confirmar eliminación de tarea';
    dialogRef.componentInstance.message = 'Está seguro de querer eliminar la tarea ?';

    // Delete task from DB
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.taskService.deleteTask(id.$oid).subscribe(result => {
            if (result.deleted) {
              this.tasks.splice(this.tasks.findIndex(item => item._id === id), 1);
              this.snackBar.open('Usuario eliminado correctamente', '', {duration: 2000});
              this.getTaskList();
            }
          },
          error => {
            console.log('Error:' + error.statusText);
          });
      }
    });
  }

  /**
   * Retrieves task list from API
   */
  getTaskList() {
    this.taskService.getTasks().subscribe(tasks => {
        this.tasks = tasks.result;
        this.dataSource = new MatTableDataSource(this.tasks);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.paginator._intl.itemsPerPageLabel = 'Tareas por página';
        });
        this.dataSource.filterPredicate = (data, filter: string): boolean => {
          return data.name.toLowerCase().includes(filter);
        };
      },
      error => {
        console.log('Error:' + error.statusText);
      });
  }

  /**
   * Applies filters to table
   * @param event filter event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  backToMenu() {
    this.router.navigateByUrl('/');
  }

  /**
   * Gets actual time
   */
  getTime() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    return date + ' ' + time;
  }
}
