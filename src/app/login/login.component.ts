import {FormControl, Validators} from '@angular/forms';
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  email = '';
  password = '';
  error: boolean;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  constructor(
    private userService: UserService,
    private router: Router
  ) {
  }

  /**
   * Logs a users into de application
   */
  login() {
    const user = {
      email: this.email,
      password: this.password
    };
    this.userService.login(user).subscribe(data => {
        this.userService.setToken(data.token);
        this.router.navigateByUrl('/');
      },
      error => {
        this.error = true;
      });
  }
}
