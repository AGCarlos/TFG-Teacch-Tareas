import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {ToDoSequenceComponent} from '../to-do-sequence/to-do-sequence.component';
import {TaskData} from '../../interfaces/interfaces.interfaces';
import {SessionService} from '../../services/session-service';
import {countTasks} from '../../utils/functions';
import {CdkDrag, CdkDragDrop, CdkDropList} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-done-free',
  templateUrl: './done-free.component.html',
  styleUrls: ['./done-free.component.scss']
})
export class DoneFreeComponent implements OnInit {

  @ViewChild(ToDoSequenceComponent) private todoComponent: ToDoSequenceComponent;

  /**
   * Done list
   */
  doneList: TaskData[] = [this.initializeTask(), this.initializeTask(), this.initializeTask(), this.initializeTask()];

  /**
   * Audio played on task completion
   */
  audio = new Audio();

  /**
   * Complete task audio
   */
  completeTaskAudio = '../../assets/sounds/task-completed.mp3';

  /**
   * Complete session audio
   */
  completeSessionAudio = '../../assets/sounds/session-completed.mp3';

  /**
   * Whether the lists should be displayed or not
   */
  displayedDoneList = [false, false, false, false];

  /**
   * Whether the sounds must be played
   */
  soundsDisabled = false;

  /**
   * Whether the animations must be played
   */
  animationsDisabled = false;

  /**
   * Emits when task is dropped
   */
  @Output() dropped = new EventEmitter<number>();

  /**
   * Emits when session is finished
   */
  @Output() finished = new EventEmitter<boolean>();

  /**
   * Task ids
   */
  private tasksNumber = 0;

  /**
   * Tasks
   */
  public tasks = [];

  /**
   * Whether the animations are activated
   */
  public doneAnimation = [false, false, false, false];
  private tasksPop: number;

  constructor(private sessionService: SessionService) {
  }

  ngOnInit() {
    if (this.sessionService.id) {
      this.sessionService.getSession(this.sessionService.id).subscribe(session => {
        this.soundsDisabled = session.result.soundsDisabled;
        this.animationsDisabled = session.result.animationsDisabled;
        this.tasks = session.result.tasks;
        this.tasksNumber = countTasks(session);
        for (let i = 0; i < 4 - this.tasksNumber; i++) {
          this.displayedDoneList.pop();
        }
      });
    }
  }

  /**
   * Limiter predicate for the working list
   * @param item Item of the list
   * @param container Container where the item is
   */
  oneElement(item: CdkDrag, container: CdkDropList) {
    if (container.data) {
      return container.data._id === 'null' ? true : false;
    }
  }

  /**
   * Drop on done zone event action
   * @param $event drop event
   */
  doneZoneDrop(event: CdkDragDrop<TaskData, any>, num: number) {
    this.doneList[num] = event.previousContainer.data[0] as unknown as TaskData;
    this.runDone(num);
  }

  /**
   * Work zone click
   */
  workZoneClick(task: TaskData, num: number) {
    this.doneList[num] = task;
    this.runDone(num);
  }

  runDone(num) {
    this.doneAnimation[num] = false;
    this.displayedDoneList[num] = true;
    this.dropped.emit(num);

    // Play completed sound
    if (!this.soundsDisabled) {
      this.audio = new Audio();
      this.audio.src = this.displayedDoneList.every(v => v === true) ? this.completeSessionAudio : this.completeTaskAudio;
      this.audio.load();
      this.audio.play();
    }

    if (this.displayedDoneList.every(v => v === true)) {
      this.finished.emit(true);
    }
  }

  initializeTask() {
    return {
      _id: 'null',
      name: '',
      image: new ArrayBuffer(1),
      helpPictos: [],
      createdAt: '',
      teacherId: ''
    };
  }
}
