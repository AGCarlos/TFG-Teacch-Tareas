import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoneFreeComponent } from './done-free.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DoneFreeComponent', () => {
  let component: DoneFreeComponent;
  let fixture: ComponentFixture<DoneFreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoneFreeComponent ],
      imports: [
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoneFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
