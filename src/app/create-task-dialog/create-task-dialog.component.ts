import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskData} from '../../interfaces/interfaces.interfaces';

@Component({
  selector: 'app-create-task-dialog',
  templateUrl: './create-task-dialog.component.html',
  styleUrls: ['./create-task-dialog.component.scss']
})
export class CreateTaskDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CreateTaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TaskData) {
  }

  ngOnInit() {
    this.data.helpPictos = [];
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close();
  }

  /**
   * Handles the image
   * @param event image event
   */
  onFileSelected(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.image = reader.result as ArrayBuffer;
      };
    }
  }

  /**
   * Handles the pictos step image
   * @param event image event
   */
  onStepFileSelected(event, i: number) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.helpPictos[i] = reader.result as ArrayBuffer;
      };
    }
  }

  /**
   * Validates the form
   */
  validate() {
    let invalid = false;

    if (!this.data.name) {
      invalid = true;
    }

    invalid = typeof this.data.image !== 'string';

    this.data.helpPictos.forEach(picto => {
      invalid = typeof picto !== 'string';
    });

    return invalid;
  }

  /**
   * Adds a step to the array of steps
   */
  addStep() {
    this.data.helpPictos.push(new ArrayBuffer(0));
  }

  /**
   * Tracks index of the step
   * @param index for step
   * @param obj step object
   */
  customTrackBy(index: number, obj: any): any {
    return index;
  }

  /**
   * Removes a step from the helpTexts
   * @param index step index
   */
  deleteStep(index: number) {
    this.data.helpPictos.splice(index, 1);
  }
}
