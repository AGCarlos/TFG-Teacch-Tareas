import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  tabColor = 'accent';
  backgroundTabColor = 'primary';

  /**
   * Actual index of the tabs
   */
  tabIndex: number;

  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.tabIndex = params.id;
    });
  }

  backToMenu() {
    this.router.navigateByUrl('/');
  }

  tabClick(event) {
    this.tabIndex = event.index;
  }
}
