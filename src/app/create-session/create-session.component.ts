import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SessionData, TaskData, UserData} from '../../interfaces/interfaces.interfaces';
import {ReplaySubject, Subject} from 'rxjs';
import {MatVerticalStepper} from '@angular/material/stepper';
import {TaskService} from '../../services/task-service';
import {takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';
import {SessionService} from '../../services/session-service';
import {UserService} from '../../services/user-service';

@Component({
  selector: 'app-create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.scss']
})
export class CreateSessionComponent implements OnInit, AfterViewInit, OnDestroy {

  /**
   * session form
   */
  tasksForm: FormGroup;

  /**
   * session form
   */
  nameForm: FormGroup;

  /**
   * Additional settings form
   */
  additionalSettings: FormGroup;

  /**
   * Tasks array
   */
  tasks1: TaskData[] = [];

  /**
   * Tasks array
   */
  tasks2: TaskData[] = [];

  /**
   * Tasks array
   */
  tasks3: TaskData[] = [];

  /**
   * Tasks array
   */
  tasks4: TaskData[] = [];

  /**
   * Student array
   */
  students: UserData[] = [];

  /**
   * Whether the session is saved
   */
  sessionSaved = false;

  @ViewChild(MatVerticalStepper) stepper: MatVerticalStepper;

  public taskFilterCtrl1: FormControl = new FormControl();
  public taskFilterCtrl2: FormControl = new FormControl();
  public taskFilterCtrl3: FormControl = new FormControl();
  public taskFilterCtrl4: FormControl = new FormControl();
  public studentFilterCtrl: FormControl = new FormControl();

  public filteredTasks1: ReplaySubject<TaskData[]> = new ReplaySubject<TaskData[]>(1);
  public filteredTasks2: ReplaySubject<TaskData[]> = new ReplaySubject<TaskData[]>(1);
  public filteredTasks3: ReplaySubject<TaskData[]> = new ReplaySubject<TaskData[]>(1);
  public filteredTasks4: ReplaySubject<TaskData[]> = new ReplaySubject<TaskData[]>(1);
  public filteredStudents: ReplaySubject<UserData[]> = new ReplaySubject<UserData[]>(1);

  /** Subject that emits when the component has been destroyed. */
  protected onDestroy = new Subject<void>();

  /**
   * Session data
   */
  private sessionData: SessionData;

  /**
   * Session Id
   */
  private sessionId: any;

  constructor(private formBuilder: FormBuilder, private taskService: TaskService,
              private router: Router, private sessionService: SessionService,
              private userService: UserService) {

    this.nameForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
    this.tasksForm = this.formBuilder.group({
      task1: ['', Validators.required],
      task2: [''],
      task3: [''],
      task4: [''],
    });
    this.additionalSettings = this.formBuilder.group({
      animationsDisabled: [false],
      soundsDisabled: [false],
      mode: ['', Validators.required],
      selection: ['', Validators.required],
    });
  }

  ngOnInit(): void {

    // Retrieve tasks
    this.taskService.getTasks().subscribe(res => {
      this.tasks1 = this.tasks2 = this.tasks3 = this.tasks4 = res.result;
    });

    // Retrieve students
    this.userService.getUsers().subscribe(res => {
      this.students = res.result;
    });

    // load the initial task list
    this.filteredTasks1.next(this.tasks1.slice());
    this.filteredTasks2.next(this.tasks2.slice());
    this.filteredTasks3.next(this.tasks3.slice());
    this.filteredTasks4.next(this.tasks4.slice());
    this.filteredStudents.next(this.students.slice());

    this.taskFilterCtrl1.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterTasks(1);
      });
    this.taskFilterCtrl2.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterTasks(2);
      });
    this.taskFilterCtrl3.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterTasks(3);
      });
    this.taskFilterCtrl4.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterTasks(4);
      });
    this.studentFilterCtrl.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterStudents();
      });
  }

  ngAfterViewInit() {
    this.stepper._getIndicatorType = () => 'number';
  }

  ngOnDestroy() {
    this.sessionService.id = this.sessionId;
  }

  protected filterTasks(i: number) {
    if (!this['tasks' + i]) {
      return;
    }
    // get the search keyword
    let search = this['taskFilterCtrl' + i].value;
    if (!search) {
      this['filteredTasks' + i].next(this['tasks' + i].slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the tasks
    this['filteredTasks' + i].next(
      this['tasks' + i].filter(task => task.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterStudents() {
    if (!this.students) {
      return;
    }
    // get the search keyword
    let search = this.studentFilterCtrl.value;
    if (!search) {
      this.filteredStudents.next(this.students.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the tasks
    this.filteredStudents.next(
      this.students.filter(user => user.name.toLowerCase().indexOf(search) > -1)
    );
  }

  createSession() {
    let teacherId = '';
    this.userService.getUserLogged().subscribe(res => {
      teacherId = res.result._id.$oid;

      this.sessionSaved = true;
      this.sessionData = {
        name: this.nameForm.value.name,
        image: this.tasksForm.get('task1').value.image,
        tasks: [
          this.tasksForm.get('task1').value._id ? this.tasksForm.get('task1').value._id.$oid : '',
          this.tasksForm.get('task2').value._id ? this.tasksForm.get('task2').value._id.$oid : '',
          this.tasksForm.get('task3').value._id ? this.tasksForm.get('task3').value._id.$oid : '',
          this.tasksForm.get('task4').value._id ? this.tasksForm.get('task4').value._id.$oid : ''
        ],
        animationsDisabled: this.additionalSettings.get('animationsDisabled').value,
        soundsDisabled: this.additionalSettings.get('soundsDisabled').value,
        mode: this.additionalSettings.get('mode').value,
        selection: this.additionalSettings.get('selection').value,
        createdAt: this.getTime(),
        teacherId,
        done: false
      };

      this.sessionService.createSession(this.sessionData).subscribe(result => {
        this.sessionId = {$oid: result.result};
      });
    });
  }

  redirectToHome() {
    this.router.navigateByUrl('/');
  }

  redirectToDashboard() {
    this.router.navigateByUrl('/dashboard');
  }

  backToSessions() {
    this.router.navigateByUrl('/sessions');
  }

  /**
   * Gets actual time
   */
  getTime() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    return date + ' ' + time;
  }
}
