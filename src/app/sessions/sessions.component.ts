import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {SessionData} from '../../interfaces/interfaces.interfaces';
import {SessionService} from '../../services/session-service';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {

  /**
   * List of sessions
   */
  sessions: SessionData[];

  displayedColumns: string[] = ['name', 'mode', 'settings', 'actions'];
  dataSource = new MatTableDataSource<SessionData>([]);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.getSessionList();
  }

  /**
   * Redirects to menu
   */
  backToMenu() {
    this.router.navigateByUrl('/');
  }

  /**
   * Retrieves task list from API
   */
  getSessionList() {
    this.sessionService.getSessions().subscribe(sessions => {
        this.sessions = sessions.result;
        this.dataSource = new MatTableDataSource(this.sessions);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.paginator._intl.itemsPerPageLabel = 'Tareas por página';
        });
        this.dataSource.filterPredicate = (data, filter: string): boolean => {
          return data.name.toLowerCase().includes(filter);
        };
      },
      error => {
        console.log('Error:' + error.statusText);
      });
  }

  /**
   * Deletes session
   * @param id Session ID to delete
   */
  deleteSession(id: any) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
    });

    dialogRef.componentInstance.title = 'Confirmar eliminación';
    dialogRef.componentInstance.message = 'Está seguro de querer eliminar la sessión ?';

    // Delete task from DB
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.sessionService.deleteSession(id.$oid).subscribe(result => {
            if (result.deleted) {
              this.sessions.splice(this.sessions.findIndex(item => item._id === id), 1);
              this.snackBar.open('Sesión eliminada correctamente', '', {duration: 2000});
              this.getSessionList();
            }
          },
          error => {
            console.log('Error:' + error.statusText);
          });
      }
    });
  }

  /**
   * Redirects to playing the session
   * @param id Session ID to play
   */
  playSession(id: string) {
    this.sessionService.id = id;
    this.router.navigateByUrl('/dashboard');
  }

  /**
   * Applies filters to table
   * @param event filter event
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * Redirects to the session creation form
   */
  createSesion() {
    this.router.navigateByUrl('/session');
  }
}
