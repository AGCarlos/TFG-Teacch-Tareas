import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {ChartsModule} from 'ng2-charts';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HttpClientModule} from '@angular/common/http';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';

import {MatStepperModule} from '@angular/material/stepper';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {NgModule} from '@angular/core';
import {routing} from './app.routing';
import {AppComponent} from './app.component';
import {CreateUserDialogComponent} from './create-user-dialog/create-user-dialog.component';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MenuComponent} from './menu/menu.component';
import {SettingsComponent} from './settings/settings.component';
import {TaskDialogComponent} from './task-dialog/task-dialog.component';
import {TaskSettingsComponent} from './task-settings/task-settings.component';
import {ToDoSequenceComponent} from './to-do-sequence/to-do-sequence.component';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {UpdateUserDialogComponent} from './update-user-dialog/update-user-dialog.component';
import {CreateTaskDialogComponent} from './create-task-dialog/create-task-dialog.component';
import {UpdateTaskDialogComponent} from './update-task-dialog/update-task-dialog.component';
import {DoneSequenceComponent} from './done-sequence/done-sequence.component';
import {CreateSessionComponent} from './create-session/create-session.component';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {SessionsComponent} from './sessions/sessions.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CookieService} from 'ngx-cookie-service';
import {AuthGuardService} from '../services/auth-guard.service';
import {AuthService} from '../services/auth-service';
import {JwtModule, JwtModuleOptions} from '@auth0/angular-jwt';
import {ToDoFreeComponent} from './to-do-free/to-do-free.component';
import {DoneFreeComponent} from './done-free/done-free.component';
import {CalendarComponent} from './calendar/calendar.component';
import {AddCalendarSessionComponent} from './add-calendar-session/add-calendar-session.component';
import {StaticticsComponent} from './statictics/statictics.component';

// tslint:disable-next-line:variable-name
const JWT_Module_Options: JwtModuleOptions = {
  config: {}
};

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ToDoSequenceComponent,
    TaskDialogComponent,
    MenuComponent,
    SettingsComponent,
    TaskSettingsComponent,
    UserSettingsComponent,
    CreateUserDialogComponent,
    ConfirmDialogComponent,
    UpdateUserDialogComponent,
    CreateTaskDialogComponent,
    UpdateTaskDialogComponent,
    DoneSequenceComponent,
    CreateSessionComponent,
    SessionsComponent,
    LoginComponent,
    RegisterComponent,
    ToDoFreeComponent,
    DoneFreeComponent,
    CalendarComponent,
    AddCalendarSessionComponent,
    StaticticsComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    DragDropModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    ReactiveFormsModule,
    routing,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule,
    MatStepperModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    NgxMatSelectSearchModule,
    ChartsModule,
    JwtModule.forRoot(JWT_Module_Options)
  ],
  entryComponents: [TaskDialogComponent, CreateUserDialogComponent, ConfirmDialogComponent,
    UpdateUserDialogComponent, CreateTaskDialogComponent, UpdateTaskDialogComponent],
  providers: [
    MatSnackBar,
    CookieService,
    AuthService,
    AuthGuardService,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
