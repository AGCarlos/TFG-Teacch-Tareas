import {Component, Inject, OnInit} from '@angular/core';
import {CreateTaskDialogComponent} from '../create-task-dialog/create-task-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SessionData} from '../../interfaces/interfaces.interfaces';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SessionService} from '../../services/session-service';

@Component({
  selector: 'app-add-calendar-session',
  templateUrl: './add-calendar-session.component.html',
  styleUrls: ['./add-calendar-session.component.scss']
})
export class AddCalendarSessionComponent implements OnInit {

  /**
   * Total students
   */
  private sessions: SessionData[] = [];

  /**
   * Variable to filter list of students
   */
  public sessionFilterCtrl: FormControl = new FormControl();
  public filteredSessions: ReplaySubject<SessionData[]> = new ReplaySubject<SessionData[]>(1);
  protected onDestroy = new Subject<void>();
  public sessionsForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CreateTaskDialogComponent>,
    private formBuilder: FormBuilder,
    private sessionService: SessionService
  ) {
  }

  ngOnInit(): void {
    this.sessionsForm = this.formBuilder.group({
      session: ['']
    });

    // Retrieve sessions
    this.sessionService.getSessions().subscribe(res => {
      this.sessions = res.result;
    });

    this.filteredSessions.next(this.sessions.slice());

    this.sessionFilterCtrl.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterStudents();
      });
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close();
  }

  protected filterStudents() {
    if (!this.sessions) {
      return;
    }
    // get the search keyword
    let search = this.sessionFilterCtrl.value;
    if (!search) {
      this.filteredSessions.next(this.sessions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredSessions.next(
      this.sessions.filter(user => user.name.toLowerCase().indexOf(search) > -1)
    );
  }

  setData() {
    return this.sessionsForm.get('session').value;
  }
}
