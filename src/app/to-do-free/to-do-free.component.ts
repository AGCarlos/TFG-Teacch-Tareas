import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SessionData, TaskClick, TaskData} from '../../interfaces/interfaces.interfaces';
import {TaskService} from '../../services/task-service';
import {SessionService} from '../../services/session-service';
import {Router} from '@angular/router';
import {CdkDragDrop} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-to-do-free',
  templateUrl: './to-do-free.component.html',
  styleUrls: ['./to-do-free.component.scss']
})
export class ToDoFreeComponent implements OnInit {

  /**
   * Emits when dropped
   */
  @Output() dropped = new EventEmitter<number>();

  /**
   * Emits when task is clicked
   */
  @Output() taskClicked: EventEmitter<TaskClick> = new EventEmitter();

  /**
   * Whether every task is active or not
   */
  activeTasks = [true, true, true, true];

  /**
   * List of tasks
   */
  tasks: TaskData[] = [];

  /**
   * Connect list for the to-do list
   */
  connectList: string;

  /**
   * Activate effects
   */
  activate = true;

  /**
   * Whether the sounds must be played
   */
  session: SessionData;

  constructor(private taskService: TaskService, private sessionService: SessionService, private router: Router) {
    this.connectList = 'workingZone';
  }

  ngOnInit() {
    if (this.sessionService.id) {
      this.sessionService.getSession(this.sessionService.id).subscribe(session => {
        this.session = session.result;
        this.taskService.getSessionTasks(session.result.tasks.filter(e => e)).subscribe(tasks => {
          session.result.tasks.forEach((value, index) => {
            tasks.result.forEach(val => {
              if (val._id.$oid === value) {
                this.tasks.push(val);
              }
            });
          });
        });
      });
    } else {
      this.router.navigateByUrl('/');
    }
  }

  /**
   * Drop function
   * @param event drop event
   */
  drop(event: CdkDragDrop<TaskData, any>, num: number) {
    this.activate = true;
    if (event.previousContainer !== event.container) {
      // Set the actual task
      this.activeTasks[num] = true;
      // Disable the task on dashboard
      this.dropped.emit(num);
    }
  }

  /**
   * Checks if the drag is disabled
   */
  dragDisabled() {
    return this.session.selection !== 'drag';
  }

  /**
   * Moves task when using click mode
   * @param task Task to move
   * @param n Task number
   */
  taskClick(task, n) {
    if (this.session.selection === 'click') {
      this.taskClicked.emit({
        task,
        number: n
      });
    }
  }

}
