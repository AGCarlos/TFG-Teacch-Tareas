import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoFreeComponent } from './to-do-free.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';

describe('ToDoFreeComponent', () => {
  let component: ToDoFreeComponent;
  let fixture: ComponentFixture<ToDoFreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToDoFreeComponent ],
      imports: [
        HttpClientTestingModule, RouterTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoFreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
