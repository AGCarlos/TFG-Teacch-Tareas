import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DoneSequenceComponent} from './done-sequence.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('DoneSequenceComponent', () => {
  let component: DoneSequenceComponent;
  let fixture: ComponentFixture<DoneSequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DoneSequenceComponent],
      imports: [
        HttpClientTestingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoneSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
