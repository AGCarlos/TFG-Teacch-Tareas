import {AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user-service';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router,
    private cookies: CookieService
  ) {
  }

  public user;

  ngOnInit() {
    this.userService.getUserLogged().subscribe(res => {
        this.user = res.result;
      },
      error => {
        console.log('Error:' + error.statusText);
      });
  }

  /**
   * Redirect to settings
   */
  redirectToTasks() {
    this.router.navigate(['/settings', 0]);
  }

  redirectToUsers() {
    this.router.navigate(['/settings', 1]);
  }

  redirectToSettings() {
    this.router.navigate(['/settings', 2]);
  }

  /**
   * Redirect to the list of sessions
   */
  redirectToCalendar() {
    this.router.navigateByUrl('/calendar');
  }

  /**
   * Redirect to the list of sessions
   */
  startSesion() {
    this.router.navigateByUrl('/sessions');
  }

  /**
   * Logout for user
   */
  public logout(): void {
    this.cookies.delete('token', '/', 'localhost');
    this.router.navigateByUrl('/login');
  }
}
