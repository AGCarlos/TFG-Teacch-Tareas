/* tslint:disable:no-string-literal */
import {Component, OnInit, ViewChild} from '@angular/core';
import {CdkDrag, CdkDragDrop, CdkDragEnter, CdkDragExit, CdkDropList} from '@angular/cdk/drag-drop';
import {SessionData, TaskClick, TaskData} from '../../interfaces/interfaces.interfaces';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {DoneFreeComponent} from '../done-free/done-free.component';
import {DoneSequenceComponent} from '../done-sequence/done-sequence.component';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {SessionService} from '../../services/session-service';
import {TaskDialogComponent} from '../task-dialog/task-dialog.component';
import {ToDoFreeComponent} from '../to-do-free/to-do-free.component';
import {ToDoSequenceComponent} from '../to-do-sequence/to-do-sequence.component';
import {countTasks, getTimeFormatted} from '../../utils/functions';
import {UserService} from '../../services/user-service';
import {LogService} from '../../services/log-service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(ToDoSequenceComponent) private todoSequenceComponent: ToDoSequenceComponent;
  @ViewChild(ToDoFreeComponent) private todoFreeComponent: ToDoFreeComponent;
  @ViewChild(DoneSequenceComponent) private doneSequenceComponent: DoneSequenceComponent;
  @ViewChild(DoneFreeComponent) private doneFreeComponent: DoneFreeComponent;

  /**
   * Working list
   */
  working = [];

  /**
   * In progress task
   */
  public actualTask: TaskData = {
    _id: 'null',
    name: '',
    image: new ArrayBuffer(1),
    helpPictos: [],
    createdAt: '',
    teacherId: ''
  };

  /**
   * Images displayed on the working zone for the done tasks
   */
  doneTasksImages: string[] = [];

  /**
   * Lists which the working zone is connected with
   */
  connectedList: string[] = ['todoList1', 'doneList1'];
  displayArrow: boolean[] = [];

  /**
   * Number of tasks
   */
  public tasksNumber = -1;

  /**
   * Actual session data
   */
  public session: SessionData;

  /**
   * Task number
   */
  private taskNum: number;

  /**
   * Whether the session is finished
   */
  public finished = false;

  /**
   * Arrow displayed number when using free mode
   */
  private displayArrowFree = 0;

  /**
   * Actual user image
   */
  userImage: ArrayBuffer;

  /**
   * Time taken to do the session
   */
  time: number;

  /**
   * Class constructor
   * @param dialog Task dialog
   */
  constructor(public dialog: MatDialog,
              private sessionService: SessionService,
              private router: Router,
              private userService: UserService,
              private logService: LogService
  ) {
  }

  ngOnInit() {
    if (this.sessionService.id) {
      this.time = performance.now();
      if (this.userService.user) {
        this.userImage = this.userService.user.image;
      }
      this.sessionService.getSession(this.sessionService.id).subscribe(session => {
        this.tasksNumber = countTasks(session);
        this.session = session.result;
      });
    } else {
      this.backToMenu();
    }

  }

  /**
   * Drop on work zone event action
   * @param event Drop event
   */
  workZoneDrop(event: CdkDragDrop<string[]>) {
    // Open the task only if the container has changed
    if (event.previousContainer !== event.container) {
      // Set the actual task
      this.actualTask = event.previousContainer.data as unknown as TaskData;
      this.working.push(this.actualTask);
      // Image handling
      this.doneTasksImages.push(this.actualTask.image as unknown as string);
      // Disable the task on todoComponent
      const taskNum = Number(event.previousContainer.id.substr(event.previousContainer.id.length - 1)) - 1;
      if (this.session.mode === 'sequence') {
        this.todoSequenceComponent.activeTasks[taskNum] = false;
        this.doneSequenceComponent.doneAnimation[taskNum] = true;
      } else {
        this.todoFreeComponent.activeTasks[taskNum] = false;
        this.doneFreeComponent.doneAnimation[taskNum] = true;
        this.connectedList = ['todoList' + (taskNum + 1), 'doneList' + (taskNum + 1)];
      }
      // Display arrow
      if (this.session.mode === 'sequence') {
        if (taskNum !== 0) {
          this.displayArrow[taskNum - 1] = false;
        }
        this.displayArrow[taskNum] = true;
      } else {
        if (this.displayArrowFree !== 0) {
          this.displayArrow[this.displayArrowFree - 1] = false;
        }
        this.displayArrow[this.displayArrowFree] = true;
        this.displayArrowFree++;
      }
      // Open the task dialog
      this.openTaskDialog();
    }
  }

  /**
   * Limiter predicate for the working list
   * @param item Item of the list
   * @param container Container where the item is
   */
  oneElement(item: CdkDrag, container: CdkDropList) {
    return container.data.length === 0;
  }

  /**
   * To be executed when a task exits the container
   * @param event exit event
   */
  exited(event: CdkDragExit<string[]>) {
    console.log('Exited', event.item.data);
  }

  /**
   * To be executed when a task enters the container
   * @param event enter event
   */
  entered(event: CdkDragEnter<string[]>) {
    console.log('Entered', event.item.data);
  }

  /**
   * Opens the task dialog
   */
  openTaskDialog(): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      height: '500px',
      width: '500px',
      data: this.actualTask,
      hasBackdrop: true,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  /**
   * Delete task from working zone when dropping on done zone
   * @param $event drop event
   */
  onZoneDrop(taskNum: number) {
    this.actualTask = {
      _id: 'null',
      name: '',
      image: new ArrayBuffer(1),
      helpPictos: [],
      createdAt: '',
      teacherId: ''
    };
    // Delete task from work zone
    this.working.pop();

    if (this.session.mode === 'sequence') {

      // Connect to the next list
      this.connectedList = ['todoList' + (taskNum + 2), 'doneList' + (taskNum + 2)];
      this.todoSequenceComponent.connectList.pop();
      this.todoSequenceComponent.connectList.unshift('');
      // activate to-do animations
      this.todoSequenceComponent.activate = true;
    }
  }

  /**
   * Deletes task when dropping on to-do zone and deletes image
   * @param $event drop event
   */
  onTodoDrop(num: number) {
    this.actualTask = {
      _id: 'null',
      name: '',
      image: new ArrayBuffer(1),
      helpPictos: [],
      createdAt: '',
      teacherId: ''
    };
    this.working.pop();
    this.doneTasksImages.pop();
    if (this.session.mode === 'sequence') {
      this.doneSequenceComponent.doneAnimation[num] = false;
    } else {
      this.doneFreeComponent.doneAnimation[num] = false;
    }
    this.displayArrowFree--;
  }

  /**
   * Sets image of the done tasks in the dashboard
   * @param num number for the image
   */
  setImage(num) {
    return this.doneTasksImages[num] === undefined ? 'assets/images/placeholder.png' : this.doneTasksImages[num];
  }

  array(n) {
    if (n !== -1) {
      return Array(n);
    }
  }

  setArrow(n) {
    return this.displayArrow[n];
  }

  /**
   * Updates actual task with to-do task
   * @param $event Task coming from child
   */
  onTodoClick(taskEvent: TaskClick) {
    if (this.working.length === 0) {
      // Set the actual task
      this.actualTask = taskEvent.task;
      this.working.push(this.actualTask);
      // Image handling
      this.doneTasksImages.push(this.actualTask.image as unknown as string);
      // Disable the task on todoComponent
      this.taskNum = taskEvent.number;
      if (this.session.mode === 'sequence') {
        this.todoSequenceComponent.activeTasks[this.taskNum] = false;
      } else {
        this.todoFreeComponent.activeTasks[this.taskNum] = false;
      }
      // Display arrow
      if (this.session.mode === 'sequence') {
        if (taskEvent.number !== 0) {
          this.displayArrow[taskEvent.number - 1] = false;
        }
        this.displayArrow[taskEvent.number] = true;
      } else {
        if (this.displayArrowFree !== 0) {
          this.displayArrow[this.displayArrowFree - 1] = false;
        }
        this.displayArrow[this.displayArrowFree] = true;
        this.displayArrowFree++;
      }
      // Open the task dialog
      this.openTaskDialog();
    }

  }

  taskClick() {
    if (this.session.selection === 'click') {
      if (this.session.mode === 'sequence') {
        this.doneSequenceComponent.workZoneClick(this.actualTask, this.taskNum);
      } else {
        this.doneFreeComponent.workZoneClick(this.actualTask, this.taskNum);
      }
    }
  }

  /**
   * Returns to menu with a confirm
   */
  backToMenuConfirm() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
    });

    dialogRef.componentInstance.title = 'Salir de la sesión actual ?';
    dialogRef.componentInstance.message = 'Se perderá el progreso actual';

    // Delete task from DB
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.router.navigateByUrl('/');
      }
    });
  }

  /**
   * Returns to menu with a confirm
   */
  backToMenu() {
    this.router.navigateByUrl('/');
  }

  backToCalendar() {
    this.router.navigateByUrl('/calendar');
  }

  finishSession() {
    this.finished = true;
    this.session.done = true;
    this.time = performance.now() - this.time;
    const logData = {
      user_id: this.userService.user ? this.userService.user._id['$oid'] : '',
      session_name: this.session.name,
      time: getTimeFormatted(this.time),
      date: this.getTime()
    };
    this.logService.createLog(logData).subscribe(() => {
    });
    this.sessionService.updateSession(this.session._id['$oid'], {done: true}).subscribe(() => {
    });
  }

  /**
   * Returns the actual time
   */
  getTime() {
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    return date + ' ' + time;
  }
}
