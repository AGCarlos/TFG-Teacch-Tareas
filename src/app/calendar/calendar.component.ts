/* tslint:disable:no-string-literal */
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {AddCalendarSessionComponent} from '../add-calendar-session/add-calendar-session.component';
import {ConfirmDialogComponent} from '../confirm-dialog/confirm-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {SessionService} from '../../services/session-service';
import {SessionData, UserData} from '../../interfaces/interfaces.interfaces';
import {UserService} from '../../services/user-service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  /**
   * Days values
   */
  days = [
    {name: 'Lunes', class: 'mon'},
    {name: 'Martes', class: 'tue'},
    {name: 'Miercoles', class: 'wed'},
    {name: 'Jueves', class: 'thurs'},
    {name: 'Viernes', class: 'fri'},
    {name: 'Sábado', class: 'sat'},
    {name: 'Domingo', class: 'sun'}
  ];

  /**
   * Actual calendar user
   */
  user: UserData;

  /**
   * Sessions data
   */
  private sessions: SessionData[] = [];

  /**
   * Total students
   */
  private students: UserData[] = [];

  /**
   * Whether the calendar is editable
   */
  editable = true;

  /**
   * Variable to filter list of students
   */
  public studentFilterCtrl: FormControl = new FormControl();
  public filteredStudents: ReplaySubject<UserData[]> = new ReplaySubject<UserData[]>(1);
  protected onDestroy = new Subject<void>();
  public studentForm: FormGroup;

  constructor(
    private router: Router,
    private userService: UserService,
    private sessionService: SessionService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
  ) {
    this.studentForm = this.formBuilder.group({
      student: ['']
    });
  }

  ngOnInit(): void {
    if (this.userService.user) {
      this.user = this.userService.user;
      this.editable = false;
    }
    // Retrieve students
    this.userService.getUsers().subscribe(res => {
      this.students = res.result;
    });

    // Retrieve sessions
    this.sessionService.getSessions().subscribe(res => {
      this.sessions = res.result;
    });

    this.filteredStudents.next(this.students.slice());

    this.studentFilterCtrl.valueChanges
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.filterStudents();
      });

  }

  protected filterStudents() {
    if (!this.students) {
      return;
    }
    // get the search keyword
    let search = this.studentFilterCtrl.value;
    if (!search) {
      this.filteredStudents.next(this.students.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredStudents.next(
      this.students.filter(user => user.name.toLowerCase().indexOf(search) > -1)
    );
  }


  /**
   * Redirects to menu
   */
  backToMenu() {
    this.router.navigateByUrl('/');
  }

  /**
   * Changes the user of the calendar
   */
  changeCalendar() {
    this.user = this.userService.user = this.studentForm.get('student').value;
  }

  /**
   * Create an array of n positions
   * @param n N positions for array
   */
  array(n) {
    if (n !== -1) {
      return Array(n);
    }
  }

  /**
   * Returns the image of the actual session
   * @param id Id for the session
   */
  getImage(id: string, day: number, position: number) {
    let image = '';
    if (this.sessions.length) {
      this.sessions.forEach((session) => {
        if (session._id['$oid'] === id) {
          image = session.image;
        }
      });
      if (image === '') {
        this.deleteCalendarSession(day, position);
      }
    }
    return image;
  }

  /**
   * Adds a session to the user calendar
   * @param calendarDay day to add the session
   */
  addSession(calendarDay: number) {
    const dialogRef = this.dialog.open(AddCalendarSessionComponent, {
      hasBackdrop: true,
      disableClose: true,
      width: '400px'
    });

    // Delete task from DB
    dialogRef.afterClosed().subscribe(sessionData => {
      if (sessionData) {
        this.user.calendar[calendarDay].push(sessionData._id.$oid);
        this.userService.updateUser(this.user._id['$oid'], {calendar: this.user.calendar}).subscribe(() => {
        });
      }
    });
  }

  /**
   * Redirects to dashboard
   * @param SessionID Session id to save
   */
  redirectToDashboard(SessionID: string) {
    const id: any = {$oid: SessionID};
    this.sessionService.id = id;
    this.userService.user = this.user;
    this.router.navigateByUrl('/dashboard');
  }

  /**
   * Returns the first word of the user name
   */
  getUserName() {
    return this.user.name.replace(/ .*/, '');
  }

  /**
   * Checks if the user is editing calendar
   * @param edit flag to disable
   */
  edit(edit: boolean) {
    this.editable = !this.editable;
    if (edit) {
      this.studentForm.disable();
    } else {
      this.studentForm.enable();
    }
  }

  /**
   * Checks visibility of component
   */
  checkVisibility() {
    return this.editable ? 'visible' : 'hidden';
  }

  /**
   * Deletes a session of the user calendar
   * @param day day to delete session
   * @param position position in day
   */
  deleteCalendarSession(day: number, position: number) {
    if (day > -1 && position > -1) {
      this.user.calendar[day].splice(position, 1);
      this.userService.updateUser(this.user._id['$oid'], {calendar: this.user.calendar}).subscribe(() => {
      });
    }
  }

  /**
   * Deletes full user calendar
   */
  deleteCalendar() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      hasBackdrop: true,
      disableClose: true,
    });

    dialogRef.componentInstance.title = 'Borrar semana completa ?';
    dialogRef.componentInstance.message = '';

    // Delete task from DB
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.user.calendar = [[], [], [], [], [], [], []];
        this.userService.updateUser(this.user._id['$oid'], {calendar: this.user.calendar}).subscribe(() => {
        });
      }
    });

  }

  setOpacity(id: string) {
    let opacity = 1;
    this.sessions.forEach(session => {
      if (session._id['$oid'] === id) {
        opacity = session.done ? 0.5 : 1;
      }
    });
    return opacity;
  }
}
