import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskData} from '../../interfaces/interfaces.interfaces';

@Component({
  selector: 'app-update-task-dialog',
  templateUrl: './update-task-dialog.component.html',
  styleUrls: ['./update-task-dialog.component.scss']
})
export class UpdateTaskDialogComponent {

  constructor(public dialogRef: MatDialogRef<UpdateTaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TaskData) {
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close();
  }

  /**
   * Handles the image
   * @param event image event
   */
  onFileSelected(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.image = reader.result as ArrayBuffer;
      };
    }
  }

  /**
   * Validates the form
   */
  validate() {
    let invalid = false;

    if (!this.data.name) {
      invalid = true;
    }
    return invalid;
  }

  /**
   * Adds a step to the array of steps
   */
  addStep() {
    this.data.helpPictos.push(new ArrayBuffer(0));
  }

  /**
   * Tracks index of the step
   * @param index for step
   * @param obj step object
   */
  customTrackBy(index: number, obj: any): any {
    return index;
  }

  /**
   * Removes a step from the helpTexts
   * @param index step index
   */
  deleteStep(index: number) {
    this.data.helpPictos.splice(index, 1);
  }

  /**
   * Handles the pictos step image
   * @param event image event
   */
  onStepFileSelected(event, i: number) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.data.helpPictos[i] = reader.result as ArrayBuffer;
      };
    }
  }
}
