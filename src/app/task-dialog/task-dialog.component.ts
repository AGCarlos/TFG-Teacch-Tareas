import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskData} from '../../interfaces/interfaces.interfaces';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent implements OnInit {

  /**
   * Actual help number text shown
   */
  helpNum = 0;

  constructor(public dialogRef: MatDialogRef<TaskDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: TaskData) {
  }

  ngOnInit() {
  }

  /**
   *  Executes when dialogs closes
   */
  closeDialog() {
    this.dialogRef.close('Task done on dialog!');
  }

  /**
   * Go to previus step
   */
  previousStep(i) {
    if (i > 0) {
      this.helpNum--;
    }
  }

  /**
   * Go to next step
   */
  nextStep(i) {
    if (this.data.helpPictos[i + 1]) {
      this.helpNum++;
    }
  }

  disableFinish() {
    if (this.data.helpPictos) {
      return this.data.helpPictos[this.helpNum + 1] !== undefined;
    }
  }

  getTaskImage() {
    let image = new ArrayBuffer(0);
    if (this.data.helpPictos && this.data.helpPictos.length) {
      image = this.data.helpPictos[this.helpNum];
    } else {
      image = this.data.image;
    }
    return image;
  }
}
