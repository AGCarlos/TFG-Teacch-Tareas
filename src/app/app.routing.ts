import {AuthGuardService as AuthGuard} from '../services/auth-guard.service';
import {CalendarComponent} from './calendar/calendar.component';
import {CreateSessionComponent} from './create-session/create-session.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MenuComponent} from './menu/menu.component';
import {RouterModule} from '@angular/router';
import {SettingsComponent} from './settings/settings.component';
import {SessionsComponent} from './sessions/sessions.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';

const appRoutes = [
  {
    path: '', component: MenuComponent, pathMatch: 'full', canActivate: [AuthGuard]
  },
  {path: 'dashboard', component: DashboardComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'session', component: CreateSessionComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'sessions', component: SessionsComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'calendar', component: CalendarComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'settings/:id', component: SettingsComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'register', component: RegisterComponent, pathMatch: 'full'},
];

export const routing = RouterModule.forRoot(appRoutes);
