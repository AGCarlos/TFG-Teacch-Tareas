/**
 * User interface
 */
export interface UserData {
  _id: string;
  name: string;
  age: number;
  role: string;
  password: string;
  image: ArrayBuffer;
  teacherId: string;
  calendar: string[][];
}

/**
 * Task interface
 */
export interface TaskData {
  _id: string;
  name: string;
  image: ArrayBuffer;
  helpPictos: ArrayBuffer[];
  createdAt: string;
  teacherId: string;

}

/**
 * Task click
 */
export interface TaskClick {
  task: TaskData;
  number: number;
}

/**
 * Session interface
 */
export interface SessionData {
  _id?: string;
  name: string;
  image: string;
  tasks: string[];
  animationsDisabled: boolean;
  soundsDisabled: boolean;
  mode: string;
  selection: string;
  createdAt: string;
  teacherId: string;
  done: boolean;
}

/**
 * Log interface
 */
export interface LogData {
  _id?: string;
  user_id: string;
  session_name: string;
  time: number;
  date: string;
}
